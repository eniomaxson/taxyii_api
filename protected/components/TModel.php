<?php
class TModel extends CActiveRecord {

	public function sendMessageAndroid($apiKey, $device, $message){
		
		$gcm = new GCMPushMessage($apiKey);
		
		$gcm->setDevices($device);
		
		$result = CJSON::decode($gcm->send($message));
		
		if (!$result || $result['success'] == 0)
			throw new CHttpException(500,CJSON::encode(array('gcm'=>'Falha ao enviar requisição')));
		 
	}
	
	
}


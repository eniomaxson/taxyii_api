<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class TController extends CController
{

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    //public $layout = '//layouts/main';
    //public $menu = array();
    //public $breadcrumbs = array();

    protected function performAjaxValidation($model, $form)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $form)
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function loadModel($class, $id)
    {
        $model = $class::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function autorizar($chave)
    {
        if (!Seguranca::model()->autorizar(Yii::app()->user->id, $chave))
        {
            throw new CHttpException(403, 'Você não possui permissão para executar esse recurso! Contate o administrador do sistema.');
        }
    }

    Const APPLICATION_ID = 'taxyii';
    
    protected function send($content = '')
    {
    	header('AddDefaultCharset UTF-8');
    	header('Content-type: application/json; charset=UTF-8');
    	header('Access-Control-Allow-Origin: *');
    	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
    
    	if (is_string($content))
    	{
    		$content = array($content);
    	}
    	echo CJSON::encode(array('data'=>CJSON::encode($content)));
    	Yii::app()->end(402);
    }
    
    protected function getRequestPayload()
    {
    	$post = file_get_contents('php://input');
    
    	$attributes = CJSON::decode($post);
    
    	return $attributes;
    }
    
    protected function setResponseError($array_msg)
    {
    	header('AddDefaultCharset UTF-8');
    	header('Content-type: application/json; charset=UTF-8');
    	header('Access-Control-Allow-Origin: *');
    	header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
    	
    	echo CJSON::encode(array(
    			'error' => $array_msg['message'],
    			'code' => $array_msg['code']
    					
    	));
    	
    	Yii::app()->end();
    }
    
    protected function registrarGCMdevice($email, $gcm_id)
    {
    	Usuario::model()->updateAll(array('gcm_device_id' => $gcm_id), 'email = :email', array(':email' => $email));
    }
    
    
}
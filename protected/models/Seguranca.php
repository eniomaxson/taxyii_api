<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SegurancaDao
 *
 * @author maxson
 */
class Seguranca extends TModel
{

    //variaveis para parametrização de permissoes de recursos
    private $perfil_id;
    private $recurso;
    private $tabela_usuario;
    private $coluna_senha;
    private $coluna_login;

    //retorna um objeto dessa classe instanciado

    public static function model()
    {
        return new Seguranca;
    }

    /* Método responsavel por autorizar o uso de um determinado recurso por um usuario autenticado */

    public function autorizar($usuario_id, $chaveRecurso)
    {
        $autorizou = false;

        try
        {
            $con = Yii::app()->db;

            $sql = "SELECT 1 FROM passageiro u 
                        INNER JOIN usuario_perfil up ON up.usuario_id = u.id
			INNER JOIN perfil p ON p.id = up.perfil_id
                        INNER JOIN perfil_recurso pr ON pr.perfil_id = p.id
                        INNER JOIN recurso r ON r.id = pr.recurso_id 
                    WHERE (u.id = :usuario_id AND r.chave = :chave)";


            $command = $con->createCommand($sql);
            $command->bindParam(":usuario_id", $usuario_id, PDO::PARAM_STR);
            $command->bindParam(":chave", $chaveRecurso, PDO::PARAM_INT);

            $data = $command->queryScalar();

            (Int) $publico = $con->createCommand("select 1 from recurso where chave = {$chaveRecurso}")->queryScalar();

            if (!empty($data) || $publico == 1)
            {
                $autorizou = true;
            }
        } catch (Exception $exc)
        {
            throw new CHttpException(500, 'Aconteceu um problema interno.');
        }

        return $autorizou;
    }

    public function rules()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'email_recuperacao_senha' => 'Email',
            'rememberMe' => 'Remember me next time',
        );
    }

    private function concederOuNegarRecurso()
    {
        $recurso_id = array_keys($this->recurso);
        $permitir = array_values($this->recurso);
        if ($this->perfil_id != null)
        {
            if ($permitir[0] === "true")
            {
                $modelPerfilRecurso = new PerfilRecurso();
                $modelPerfilRecurso->attributes = array('perfil_id' => (Int) $this->perfil_id, 'recurso_id' => (Int) $recurso_id[0]);
                $modelPerfilRecurso->save(false);
            } else
            {
                PerfilRecurso::model()->deleteAll('perfil_id = :perfil_id and recurso_id = :recurso_id', array(':perfil_id' => (Int) $this->perfil_id, ':recurso_id' => (Int) $recurso_id[0]));
            }
        } else
        {
            if ($permitir[0] === "true")
            {
                Recurso::model()->updateByPk($recurso_id, array('publico' => 1));
            } else
            {
                Recurso::model()->updateByPk($recurso_id, array('publico' => 0));
            }
        }
    }

    public function setPerfisErecursos($perfil, $recurso)
    {
        $this->perfil_id = $perfil;
        $this->recurso = $recurso;
        $this->concederOuNegarRecurso();
    }

    public function validaRecursoPerfil($perfil, $recurso)
    {
        $validado = PerfilRecurso::model()->exists('perfil_id=:perfil_id AND recurso_id=:recurso_id', array(':perfil_id' => $perfil, ':recurso_id' => $recurso));
        return $validado;
    }

    public static function autenticarAndroid($key)
    {
    	$key = sha1(trim($key));
    	
    	$taxista = Yii::app ()->params ['project_taxista'];
    	
    	$passageiro = Yii::app ()->params ['project_passageiro'];
		
		if (($key != $taxista) and ($key != $passageiro))
        	throw new CHttpException ( 403, CJSON::encode ( array ('acesso' => utf8_decode('Acesso n�o permitido'))));
    }

}

?>

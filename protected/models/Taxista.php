
<?php

/**
 * This is the model class for table "taxista".
 *
 * The followings are the available columns in table 'taxista':
 * @property integer $id
 * @property string $numero_licenca
 * @property string $documento_frente
 * @property string $documento_Verso
 * @property integer $cidade_id
 * @property string $cpf
 * @property integer $passageiro_id
 * @property string $posicao_atual
 *
 * The followings are the available model relations:
 * @property Corrida[] $corridas
 * @property Taxi[] $taxis
 * @property Cidade $cidade
 * @property Passageiro $passageiro
 * @property FormaPagamento[] $formaPagamentos
 */
class Taxista extends TModel
{

    public $descricao_cidade;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'taxista';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('numero_licenca, cidade_id, cpf, passageiro_id', 'required'),
            array('cidade_id, passageiro_id', 'numerical', 'integerOnly' => true),
            array('numero_licenca', 'length', 'max' => 40),
            array('documento_frente, documento_verso', 'length', 'max' => 100),
            array('cpf', 'length', 'max' => 11),
            array('posicao_atual,descricao_cidade', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, numero_licenca, documento_frente, documento_verso, cidade_id, cpf, passageiro_id, posicao_atual', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'corridas' => array(self::HAS_MANY, 'Corrida', 'taxista_id'),
            'taxis' => array(self::HAS_MANY, 'Taxi', 'taxista_id'),
            'cidade' => array(self::BELONGS_TO, 'Cidade', 'cidade_id'),
            'passageiro' => array(self::BELONGS_TO, 'Usuario', 'passageiro_id'),
            'formaPagamentos' => array(self::MANY_MANY, 'FormaPagamento', 'taxista_forma_pagamento(taxista_id, forma_pagamento_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'numero_licenca' => 'Numero Licenca',
            'documento_frente' => 'Documento Frente',
            'documento_Verso' => 'Documento Verso',
            'cidade_id' => 'Cidade',
            'cpf' => 'Cpf',
            'passageiro_id' => 'Passageiro',
            'posicao_atual' => 'Posicao Atual',
            'descricao_cidade' => 'Descricao cidade',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('numero_licenca', $this->numero_licenca, true);
        $criteria->compare('documento_frente', $this->documento_frente, true);
        $criteria->compare('documento_Verso', $this->documento_Verso, true);
        $criteria->compare('cidade_id', $this->cidade_id);
        $criteria->compare('cpf', $this->cpf, true);
        $criteria->compare('passageiro_id', $this->passageiro_id);
        $criteria->compare('posicao_atual', $this->posicao_atual, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getPosicaoAtual($lista_negacao = array())
    {
        $where = 'where p.bloqueio = 0 ';

        if (count($lista_negacao) > 0)
        {
            $where = 'and t.id not in (' . implode(',', $lista_negacao) . ')';
        }


        $db = Yii::app()->db;
        $sql = "SELECT t.*, c.nome as cidade, p.nome,p.celular,p.gcm_device_id FROM taxista t
                INNER JOIN passageiro p ON p.id = t.passageiro_id
                INNER JOIN cidade c ON c.id = t.cidade_id
                 $where
                ORDER BY c.nome
                ";

        $taxistas = $db->createCommand($sql)->queryAll();

        foreach ($taxistas as $taxista)
        {
            if (!$this->taxiOcupado($taxista))
            {
                $taxis[] = $taxista;
            }
        }
        $taxis = (isset($taxis)) ? $taxis : null;
        return $taxis;
    }

    private function taxiOcupado($taxista)
    {
        $ocupado = true;

        if ($taxista['posicao_atual'])
            $ocupado = false;

        return $ocupado;
    }

    // Metodo retorna um array contendo os dados  do taxista mais proximo do passageiro

    public function getTaxiToPassageiro($coordenada_passageiro, $taxistas_negarao = array())
    {
        $taxistas = $this->getPosicaoAtual($taxistas_negarao);
		
        $distancias = $this->getDistanciaGoogleMaps($taxistas, $coordenada_passageiro);

        if ($distancias == null)
            return null;

        $indice_menor_distancia = 0;

        $indice = null;

        foreach ($distancias['rows'] as $key => $values)
        {
            $valor_atual = preg_replace('/[^.0-9]/', '', $values['elements'][0]['distance']['text']);
			
			if($valor_atual <= 30){
	            if ($indice_menor_distancia == 0)
	            {
	                $indice_menor_distancia = $valor_atual;
	                $indice = $key;
	            } elseif ($indice_menor_distancia > $valor_atual)
	            {
	                $indice_menor_distancia = $valor_atual;
	                $indice = $key;
	            }
			}
        }
		
        if($valor_atual > 30)
        	return  null;
        
        return $taxistas[$indice];
    }

    public function getDistanciaGoogleMaps($taxistas, $coordenada_passageiro)
    {
        $url = "http://maps.googleapis.com/maps/api/distancematrix/json?";
        $origins = "";
        $destinations = $coordenada_passageiro;

        if (count($taxistas) > 0)
        {
            foreach ($taxistas as $value)
            {
                if (empty($origins))
                {
                    $origins = $value['posicao_atual'];
                } else
                {
                    $origins .= '|' . $value['posicao_atual'];
                }
            }

            $url .= 'origins=' . $origins . '&destinations=' . $destinations . '&sensor=false';

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = utf8_decode(curl_exec($ch));

            curl_close($ch);
			
            $result = CJSON::decode($result);
            
            return $result;
        }
        return null;
    }

    public function sendPedidoToTaxista($taxista, $corrida)
    {
        $passageiro = Usuario::model()->findByPk($corrida->passageiro_id);
       
        $message = json_encode(array(
            'tipo' => 'solicitacao_corrida',
            #'passageiro_id' => $passageiro->id,
            'passageiro_nome' => $passageiro->nome,
            #'passageiro_gcm_id'=> $passageiro->gcm_device_id,
            'passageiro_celular' => $passageiro->celular,
            'cidade' => $corrida->cidade,
            'bairro' => $corrida->bairro,
            'rua' => $corrida->rua,
            'numero' => $corrida->numero,
            'ponto_referencia' => $corrida->ponto_referencia,
            'ponto_partida' => $corrida->ponto_partida,
            'corrida_id' => (int) $corrida->id,
                )
        );
		        
        $device = array($taxista['gcm_device_id']);
        $apiKey = Yii::app()->params['apikey_taxista'];
        $this->sendMessageAndroid($apiKey, $device, $message);
    }

    public function negarCorrida($corrida_id, $usuario_id)
    {
        if (!$corrida_id || !$usuario_id)
            return false;

        $corrida = Corrida::model()->findByPk($corrida_id);
		
        $taxista = $this->model()->find('passageiro_id=?',array($usuario_id));
        
        $corridaNegada = new CorridaNegadaTaxista;

        $corridaNegada->taxista_id = $taxista->id;
        $corridaNegada->corrida_id = $corrida->id;

        $transaction = $corrida->getDbConnection()->beginTransaction();
        try
        {
            $corridaNegada->save(false);
            $corrida->save(false);
            
            $transaction->commit();
            
        } catch (CException $e)
        {
            $transaction->rollback();
            throw new CHttpException(500, CJSON::encode( $corridaNegada->getErrors()));
        }
    }

    public function getListaTaxistaNegou($corrida_id)
    {

        $list_taxista_negou = CorridaNegadaTaxista::model()->findAll('corrida_id=?', array($corrida_id));

        if (count($list_taxista_negou) > 0)
        {
            foreach ($list_taxista_negou as $taxista_negou)
            {
                $lista[] = $taxista_negou->taxista_id;
            }
        }
        
        if (isset($lista))
            	return $lista;

        return array();
    }

    public function aceitarCorrida($corrida_id, $usuario_id)
    {
        $corrida = Corrida::model()->findByPk($corrida_id);
        
        $taxista = Taxista::model()->find('passageiro_id = ?',array($usuario_id));
        
        $corrida->status = 'A';
        $corrida->taxista_id = $taxista->id;
        
        $taxista->posicao_atual = null;

        $taxista->save(false);
        $corrida->save(false);
        
        $passageiro = $corrida->passageiro;
        
        $message = CJSON::encode(array(
        	'tipo'=>'corrida_aceita',	
            'corrida_id' => $corrida->id,
            'taxista_id' => $taxista->id,
            'taxista_nome' => $taxista->passageiro->nome)
        );

        $apiKey = Yii::app()->params['apikey_passageiro'];

        $device = array($passageiro->gcm_device_id);

        $this->sendMessageAndroid($apiKey, $device, $message);
         
    }
    
    public function concluirCorrida($corrida_id){
    	
    	if (!$corrida_id)
    		throw new CHttpException(404, CJSON::encode(array('corrida_id'=>"Corrida n�o encontrada")));
    	
    	$corrida = Corrida::model()->findByPk($corrida_id);	
    	
    	try{		
    		$corrida->status = "C";
    		$corrida->save();
    	}catch(CException $e){
    		throw new CHttpException(500, CJSON::encode($corrida->getErrors()));	
    	}
    	
    }
    
    public function listagemCorrida($data_inicio, $data_fim,$taxista_id){
    	
    	$db = Yii::app()->db;
    	$dados = array();
    	
    	$sql = "select 
    				p.nome as passageiro_nome,
    				DATE_FORMAT(c.data_corrida,'%d/%m/%Y %H:%i:%s') as  data_corrida, 
    				c.rua, 
    				c.status 
    			from corrida c
				inner join passageiro p on p.id = c.passageiro_id
				where c.data_corrida between :data_inicio and :data_fim and c.taxista_id = :taxista_id and c.status = 'C'";
    	
    	$command = $db->createCommand($sql);
		$command->bindParam(':data_inicio', $data_inicio,PDO::PARAM_STR);
		$command->bindParam(':data_fim', $data_fim,PDO::PARAM_STR);    	
		$command->bindParam(':taxista_id', $taxista_id,PDO::PARAM_INT);		
		$data = $command->queryAll();
		
		$condition = "data_negacao between :data_inicio and :data_fim and taxista_id=:taxista_id";
		$params= array(':taxista_id'=>$taxista_id,':data_inicio'=>$data_inicio, ':data_fim'=>$data_fim);

		$dados['corrida'] = $data;
		$dados['total_corrida_concluida'] = count($data);
		$dados['total_corrida_negada'] = CorridaNegadaTaxista::model()->count($condition,$params);
		
		return $dados;
    }
}

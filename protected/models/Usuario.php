<?php

/**
 * This is the model class for table "passageiro".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $id
 * @property string $nome
 * @property string $sobrenome
 * @property string $sexo
 * @property string $data_nascimento
 * @property string $email
 * @property string $cpf
 * @property string $celular
 * @property string $login
 * @property string $senha
 * @property integer $usuario_mestre
 * @property integer $bloqueio
 * @property string $ultimo_acesso
 *
 * The followings are the available model relations:
 * @property Motorista $motorista
 * @property UsuarioEmpresa[] $usuarioEmpresas
 * @property UsuarioPerfil[] $usuarioPerfils
 */
class Usuario extends TModel
{

    //public $confirmacao;
    public $verifyCode;
    public $confirmacao;
    public $rememberMe;
    private $_identity;
    //Cidade / RN
    public $descricao_cidade;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Usuario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'passageiro';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome,sobrenome, email, celular, senha', 'required', 'on' => 'crud,primeiro_acesso'),
            array('senha', 'length', 'min' => 6, 'on' => 'crud,primeiro_acesso'),
            array('email', 'unique', 'on' => 'crud,primeiro_acesso', 'message' => 'Email informado j� est� cadastrado!'),
            array('nome,cidade_id, email', 'length', 'max' => 60),
            array('sexo', 'length', 'max' => 1, 'on' => 'crud,primeiro_acesso'),
            //array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'primeiro_acesso,recupera_senha'),
            array('email', 'email', 'message' => 'Informe um email v�lido!'),
            array('celular', 'length', 'max' => '15'),
            //login
            array('usuario','unique','message'=>'Usu�rio informado j� est� cadastrado!'),
            array('email, senha', 'required', 'on' => 'login'),
            array('senha', 'authenticate', 'on' => 'login'),
            array('rememberMe', 'boolean',),
            //array('cpf', 'length', 'max' => 11, 'on' => 'crud'),
            array('confirmacao', 'compare', 'compareAttribute' => 'senha', 'message' => 'Confirma��o deve ser igual a senha!', 'on' => 'alterar_senha'),
            //array('data_nascimento', 'date', 'format' => 'yyyy-MM-dd', 'allowEmpty' => true),
            array('gcm_device_id', 'length', 'max'=>255),
            array('usuario,usuario_mestre,bloqueio,descricao_cidade,cidade_id','safe'),
            //array('confirmacao', 'compare', 'compareAttribute' => 'senha', 'message' => 'Senha não bate com a confirmação!'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, nome, sexo,sobrenome, email, celular, senha, usuario_mestre, bloqueio, foto, cidade_id, licenca_taxista_id, ultimo_acesso', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'corridas' => array(self::HAS_MANY, 'Corrida', 'passageiro_id'),
            'enderecoFavoritos' => array(self::HAS_MANY, 'EnderecoFavorito', 'passageiro_id'),
            'cidade' => array(self::BELONGS_TO, 'Cidade', 'cidade_id'),
            'taxistas' => array(self::HAS_MANY, 'Taxista', 'passageiro_id'),
            'usuarioPerfils' => array(self::HAS_MANY, 'UsuarioPerfil', 'Usuario_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'foto' => 'Foto',
            'nome' => 'Nome',
            'sobrenome' => 'Sobrenome',
            'sexo' => 'Sexo',
            'cidade_id' => 'Cidade',
            'email' => 'Email',
            'senha' => 'Senha',
            'celular' => 'Celular',
            'bloqueio' => 'Bloqueio',
            'ultimo_acesso' => 'Ultimo Acesso',
            'gcm_device_id' => 'Gcm Device',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('sobrenome', $this->sobrenome, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('cidade_id', $this->cidade_id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('senha', $this->senha, true);
        $criteria->compare('celular', $this->celular, true);
        $criteria->compare('bloqueio', $this->bloqueio);
        $criteria->compare('ultimo_acesso', $this->ultimo_acesso);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function authenticate()
    {
        if (!$this->hasErrors())
        {
            $this->_identity = new TUserIdentity($this->email, $this->senha);
            if (!$this->_identity->authenticate())
                $this->addError('senha', 'Usuário ou senha incorreto.');
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null)
        {
            $this->_identity = new TUserIdentity($this->email, $this->senha);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === TUserIdentity::ERROR_NONE)
        {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else
            return false;
    }

//    protected function beforeValidate()
//    {
//        if (!empty($this->data_nascimento))
//        {
//            $this->data_nascimento = Utils::formatDate($this->data_nascimento, 'db');
//        }
//        return parent::beforeValidate();
//    }
//
//    protected function beforeSave()
//    {
//        if ($this->data_nascimento)
//        {
//            $this->data_nascimento = Utils::formatDate($this->data_nascimento);
//        }
//        return parent::beforeSave();
//    }

    public function validatePassword($password)
    {
        return $this->hashPassword($password) === $this->senha;
    }

    public function hashPassword($password)
    {
        return md5($password);
    }

    public function setEmpresaUsuario()
    {
        $empresa_id = null;
        foreach ($this->usuarioEmpresa as $value)
        {
            $value->bloqueio = (int) $value->bloqueio;
            if (!$value->bloqueio)
            {
                $empresa_id = $value->empresa_id;
            }
        }
        return $empresa_id;
    }

    public function confirmAcess($email, $senha)
    {
        //Caso acombinação passada exista  na base o usuario será ativado
        $usuario = $this->model()->find('email = :email and senha = :senha', array(':email' => trim($email), ':senha' => trim($senha)));
        $confirmado = false;

        if (!empty($usuario))
        {
            $usuario->bloqueio = 0;
            $usuario->save(false);
            return !$confirmado;
        }
        return $confirmado;
    }

    public function sendEmailConfirmacao($message)
    {
        $mail = new Mail($this, utf8_decode('Confirma��o de acesso!'), $message);
    }
    
}
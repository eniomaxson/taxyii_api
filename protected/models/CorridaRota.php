<?php

/**
 * This is the model class for table "corrida_rota".
 *
 * The followings are the available columns in table 'corrida_rota':
 * @property integer $id
 * @property string $data
 * @property integer $latitude
 * @property integer $longitude
 * @property string $precisao
 * @property integer $corrida_id
 *
 * The followings are the available model relations:
 * @property Corrida $corrida
 */
class CorridaRota extends TModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'corrida_rota';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('latitude, longitude, precisao, corrida_id', 'required'),
			array('latitude, longitude, corrida_id', 'numerical', 'integerOnly'=>true),
			array('precisao', 'length', 'max'=>45),
			array('data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data, latitude, longitude, precisao, corrida_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'corrida' => array(self::BELONGS_TO, 'Corrida', 'corrida_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data' => 'Data',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'precisao' => 'Precisao',
			'corrida_id' => 'Corrida',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('latitude',$this->latitude);
		$criteria->compare('longitude',$this->longitude);
		$criteria->compare('precisao',$this->precisao,true);
		$criteria->compare('corrida_id',$this->corrida_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CorridaRota the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

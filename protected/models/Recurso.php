<?php

/**
 * This is the model class for table "recurso".
 *
 * The followings are the available columns in table 'recurso':
 * @property integer $id
 * @property string $nome
 * @property integer $chave
 * @property string $titulo
 * @property string $link
 * @property integer $publico
 * @property string $icone
 * @property integer $recurso_categoria_id
 *
 * The followings are the available model relations:
 * @property PerfilRecurso[] $perfilRecursos
 * @property RecursoCategoria $recursoCategoria
 */
class Recurso extends TModel
{

    private $chaveRecurso = array(
        'cadastro_cliente' => 1,
        'cadastro_motorista' => 2,
        'cadastro_veiculo' => 3,
    );
    
    public function getChaveRecurso($index)
    {
        return $this->chaveRecurso[$index];
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Recurso the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'recurso';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome, chave, titulo, link, publico', 'required'),
            array('chave, publico, recurso_categoria_id', 'numerical', 'integerOnly' => true),
            array('nome, link, icone', 'length', 'max' => 45),
            array('titulo', 'length', 'max' => 60),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, nome, chave, titulo, link, publico, icone, recurso_categoria_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'perfilRecursos' => array(self::HAS_MANY, 'PerfilRecurso', 'recurso_id'),
            'recursoCategoria' => array(self::BELONGS_TO, 'RecursoCategoria', 'recurso_categoria_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'chave' => 'Chave',
            'titulo' => 'Titulo',
            'link' => 'Link',
            'publico' => 'Publico',
            'icone' => 'Icone',
            'recurso_categoria_id' => 'Recurso Categoria',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('chave', $this->chave);
        $criteria->compare('titulo', $this->titulo, true);
        $criteria->compare('link', $this->link, true);
        $criteria->compare('publico', $this->publico);
        $criteria->compare('icone', $this->icone, true);
        $criteria->compare('recurso_categoria_id', $this->recurso_categoria_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function recursosDisponiveis($usuario_id = null)
    {
        $recursos = null;
        $sql = "SELECT r.* FROM recurso r WHERE r.publico = 1";
        try
        {
            $con = Yii::app()->db;
            if ($usuario_id)
            {
                $sql .= " UNION 
                    SELECT r.* FROM recurso r 
                        INNER JOIN perfil_recurso pr ON pr.recurso_id = r.id
                        INNER JOIN perfil p ON p.id = pr.perfil_id
                        INNER JOIN usuario_perfil up ON up.perfil_id = p.id
                        INNER JOIN passageiro u ON u.id = up.usuario_id
                    WHERE u.id = :usuario_id  ORDER BY 'r.titulo'";
            }
            $command = $con->createCommand($sql);

            if ($usuario_id != null)
            {
                $command->bindParam(':usuario_id', $usuario_id, PDO::PARAM_INT);
            }
            
            $recursos = $command->queryAll();
        } catch (Exception $exc)
        {
            throw new CHttpException(500, 'Erro aqui interno');
        }
        return $recursos;
    }

}
<?php

/**
 * This is the model class for table "empresa".
 *
 * The followings are the available columns in table 'empresa':
 * @property integer $id
 * @property string $nome
 * @property string $cnpj
 * @property integer $bloqueio
 * @property string $empresacol
 *
 * The followings are the available model relations:
 * @property Conveniado[] $conveniados
 * @property Perfil[] $perfils
 * @property UsuarioEmpresa[] $usuarioEmpresas
 */
class Empresa extends TModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'empresa';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, nome, cnpj', 'required'),
            array('id, bloqueio', 'numerical', 'integerOnly' => true),
            array('nome, cnpj', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nome, cnpj, bloqueio, empresacol', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'conveniados' => array(self::HAS_MANY, 'Conveniado', 'empresa_id'),
            'perfils' => array(self::HAS_MANY, 'Perfil', 'empresa_id'),
            'usuarioEmpresa' => array(self::HAS_MANY, 'UsuarioEmpresa', 'empresa_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'cnpj' => 'CNPJ',
            'bloqueio' => 'Bloqueio',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('cnpj', $this->cnpj, true);
        $criteria->compare('bloqueio', $this->bloqueio);
        $criteria->compare('empresacol', $this->empresacol, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Empresa the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}

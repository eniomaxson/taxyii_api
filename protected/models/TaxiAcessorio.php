<?php

/**
 * This is the model class for table "taxi_acessorio".
 *
 * The followings are the available columns in table 'taxi_acessorio':
 * @property integer $id
 * @property integer $taxi_id
 * @property integer $acessorio_id
 *
 * The followings are the available model relations:
 * @property Acessorio $acessorio
 * @property Taxi $taxi
 */
class TaxiAcessorio extends TModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'taxi_acessorio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('taxi_id, acessorio_id', 'required'),
			array('taxi_id, acessorio_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, taxi_id, acessorio_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'acessorio' => array(self::BELONGS_TO, 'Acessorio', 'acessorio_id'),
			'taxi' => array(self::BELONGS_TO, 'Taxi', 'taxi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'taxi_id' => 'Taxi',
			'acessorio_id' => 'Acessorio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('taxi_id',$this->taxi_id);
		$criteria->compare('acessorio_id',$this->acessorio_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TaxiAcessorio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

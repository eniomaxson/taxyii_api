<?php

/**
 * This is the model class for table "corrida".
 *
 * The followings are the available columns in table 'corrida':
 * @property integer $id
 * @property integer $passageiro_id
 * @property integer $taxista_id
 * @property string $numero
 * @property string $ponto_partida
 * @property string $ponto_referencia
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Taxista $taxista
 * @property Passageiro $passageiro
 * @property CorridaNegadaTaxista[] $corridaNegadaTaxistas
 * @property CorridaRota[] $corridaRotas
 */
class Corrida extends TModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'corrida';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ponto_partida, passageiro_id', 'required'),
            array('passageiro_id, taxista_id, endereco_favorito_id', 'numerical', 'integerOnly'=>true),
            array('ponto_partida, numero', 'length', 'max'=>45),
            array('status', 'length', 'max'=>1),
            array('cidade, rua,bairro', 'length', 'max'=>80),
            array('ponto_referencia', 'length', 'max'=>60),
            array('data_corrida', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, ponto_partida, rua, status, data_corrida, passageiro_id, taxista_id, endereco_favorito_id, cidade, bairro, numero, ponto_referencia', 'safe', 'on'=>'search'),
            array('passageiro_id','corridaJaSolicitada'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'enderecoFavorito' => array(self::BELONGS_TO, 'EnderecoFavorito', 'endereco_favorito_id'),
            'taxista' => array(self::BELONGS_TO, 'Taxista', 'taxista_id'),
            'passageiro' => array(self::BELONGS_TO, 'Usuario', 'passageiro_id'),
            'corridaNegadaTaxistas' => array(self::HAS_MANY, 'CorridaNegadaTaxista', 'corrida_id'),
            'rotas' => array(self::HAS_MANY, 'CorridaRota', 'corrida_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ponto_partida' => 'Ponto Partida',
            'status' => 'Status',
            'data_corrida' => 'Data Corrida',
            'passageiro_id' => 'Passageiro',
            'taxista_id' => 'Taxista',
            'endereco_favorito_id' => 'Endereco Favorito',
            'cidade' => 'Cidade',
            'bairro' => 'Bairro',
            'numero' => 'Numero',
        	'rua'=>'Rua',
            'ponto_referencia' => 'Ponto Referencia',
        );
    }
    
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getCorridaFromTaxista($usuario_id){
	$taxista = Taxista::model()->find('passageiro_id = ?',array($usuario_id));
	
	if(!isset($taxista->id))
		return null;	

	$corrida = Corrida::model()->findBySql("select * from corrida where status = 'A' and taxista_id = :taxista_id order by id desc limit 1",array(':taxista_id'=>$taxista->id));
	
	if(!isset($corrida->id))
		return null;

	return $corrida;	
    }
    
    public function corridaJaSolicitada($attributes,$params){
        $parametros = array(':passageiro_id'=>  $this->passageiro_id, ':status'=>'P');
        
        $ja_solicitada = $this->exists('passageiro_id = :passageiro_id and status = :status',$parametros);   
    
        if($ja_solicitada)
            $this->addError ('passageiro_id', 'Voc� possui uma corrida pendente, aguarde que logo um motorista entrar em contato!');
    }
}

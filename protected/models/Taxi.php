<?php

/**
 * This is the model class for table "taxi".
 *
 * The followings are the available columns in table 'taxi':
 * @property integer $id
 * @property string $placa
 * @property integer $ano
 * @property integer $taxista_id
 * @property integer $taxi_modelo_id
 *
 * The followings are the available model relations:
 * @property Taxista $taxista
 * @property TaxiModelo $taxiModelo
 * @property TaxiAcessorio[] $taxiAcessorios
 */
class Taxi extends TModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'taxi';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('placa, ano, taxista_id, taxi_modelo_id, principal', 'required'),
            array('ano, taxista_id, taxi_modelo_id, principal,com_ar, ativo', 'numerical', 'integerOnly' => true),
            array('placa', 'length', 'max' => 7),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, placa, ano, taxista_id, taxi_modelo_id, com_ar, principal, ativo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'taxista' => array(self::BELONGS_TO, 'Taxista', 'taxista_id'),
            'taxiModelo' => array(self::BELONGS_TO, 'TaxiModelo', 'taxi_modelo_id'),
            'taxiAcessorios' => array(self::HAS_MANY, 'TaxiAcessorio', 'taxi_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'placa' => 'Placa',
            'ano' => 'Ano',
            'taxista_id' => 'Taxista',
            'taxi_modelo_id' => 'Taxi Modelo',
            'principal' => 'Principal',
            'ativo' => 'Ativo',
            'com_ar' => 'Com ar',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('placa', $this->placa, true);
        $criteria->compare('ano', $this->ano);
        $criteria->compare('taxista_id', $this->taxista_id);
        $criteria->compare('taxi_modelo_id', $this->taxi_modelo_id);
        $criteria->compare('principal', $this->principal);
        $criteria->compare('ativo', $this->ativo);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Taxi the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function listTaxiToTaxista($idTaxista)
    {
        $db = Yii::app()->db;

        $sql = "SELECT 
                tx.id, 
                tx.ano, 
                tx.principal,
                tx.taxista_id,
                tx.placa,
                tx.com_ar,
                m.nome as marca,
                m.id as marca_id,
                md.nome as modelo,
                md.id as taxi_modelo_id
                FROM taxi_marca m
                INNER JOIN taxi_modelo md on m.id = md.taxi_marca_id
                INNER JOIN taxi tx ON md.id = tx.taxi_modelo_id
                INNER JOIN taxista t ON t.id = tx.taxista_id
                WHERE tx.taxista_id = :taxista_id AND ativo = 1";
        $command = $db->createCommand($sql);

        $command->bindParam(':taxista_id', $idTaxista, PDO::PARAM_INT);

        return $command->queryAll();
    }

    public function desativar($id)
    {
        $model = Taxi::model()->findByPk($id);
        if (!empty($model->id))
        {
            $model->ativo = 0;
            return $model->save(false);
        }
        return false;
    }
  
}

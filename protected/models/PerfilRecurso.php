<?php

/**
 * This is the model class for table "perfil_recurso".
 *
 * The followings are the available columns in table 'perfil_recurso':
 * @property integer $id
 * @property integer $recurso_id
 * @property integer $perfil_id
 *
 * The followings are the available model relations:
 * @property Perfil $perfil
 * @property Recurso $recurso
 */
class PerfilRecurso extends TModel
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PerfilRecurso the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'perfil_recurso';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('recurso_id, perfil_id', 'required'),
            array('recurso_id, perfil_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, recurso_id, perfil_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'perfil' => array(self::BELONGS_TO, 'Perfil', 'perfil_id'),
            'recurso' => array(self::BELONGS_TO, 'Recurso', 'recurso_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'recurso_id' => 'Recurso',
            'perfil_id' => 'Perfil',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('recurso_id', $this->recurso_id);
        $criteria->compare('perfil_id', $this->perfil_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
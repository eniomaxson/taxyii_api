<?php

class FormaPagamento extends TModel
{

    public function tableName()
    {
        return 'forma_pagamento';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('descricao', 'required'),
            array('descricao', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, descricao', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'taxistas' => array(self::MANY_MANY, 'Taxista', 'taxista_forma_pagamento(forma_pagamento_id, taxista_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'descricao' => 'Descricao',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('descricao', $this->descricao, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function listFrmPagamentoToTaxista($taxista_id)
    {
        $sql = "select 
                fp.*,
                (select 
                    count(*)      
                from taxista_forma_pagamento tfp 
                where tfp.taxista_id = :taxista_id and tfp.forma_pagamento_id = fp.id) as aceita
                from forma_pagamento fp 
                ";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(':taxista_id', $taxista_id, PDO::PARAM_INT);
        return $command->queryAll();
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}

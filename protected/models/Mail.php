<?php

class Mail
{

    public function __construct($usuario, $assunto, $message)
    {
        $this->send($usuario, $assunto, $message);
    }

    public function send($usuario, $assunto, $message)
    {
        $enviado = false;
        $mail = Yii::app()->smtpmail;
        $mail->Subject = $assunto;
        $mail->SetFrom(Yii::app()->params['adminEmail'], 'Enio Maxson');
        $mail->AddAddress($usuario->email);
        $mail->MsgHTML($message);
        try
        {
            $mail->Send();
        } catch (Exception $e)
        {
            throw new CHttpException(500,CJSON::encode(array('email'=>'N�o foi possivel enviar o email, por favor tente novamente!')));
        }
        return !$enviado;
    }

}

?>

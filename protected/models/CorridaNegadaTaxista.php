<?php

/**
 * This is the model class for table "corrida_negada_taxista".
 *
 * The followings are the available columns in table 'corrida_negada_taxista':
 * @property integer $taxista_id
 * @property integer $corrida_id
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property Corrida $corrida
 * @property Taxista $taxista
 */
class CorridaNegadaTaxista extends TModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'corrida_negada_taxista';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('taxista_id, corrida_id', 'required'),
            array('taxista_id, corrida_id', 'numerical', 'integerOnly' => true),
        	array('data_negacao','safe'),	
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('taxista_id,data_negacao, corrida_id, id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'corrida' => array(self::BELONGS_TO, 'Corrida', 'corrida_id'),
            'taxista' => array(self::BELONGS_TO, 'Taxista', 'taxista_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'taxista_id' => 'Taxista',
            'corrida_id' => 'Corrida',
            'id' => 'ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('taxista_id', $this->taxista_id);
        $criteria->compare('corrida_id', $this->corrida_id);
        $criteria->compare('id', $this->id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CorridaNegadaTaxista the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}

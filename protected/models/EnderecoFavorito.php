<?php

/**
 * This is the model class for table "endereco_favorito".
 *
 * The followings are the available columns in table 'endereco_favorito':
 * @property integer $id
 * @property integer $passageiro_id
 * @property string $numero
 * @property string $ponto_referencia
 * @property string $complemento
 * @property string $latitude
 * @property string $longitude
 *
 * The followings are the available model relations:
 * @property Passageiro $passageiro
 */
class EnderecoFavorito extends TModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'endereco_favorito';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome, rua, bairro, cidade, numero, coordenada, passageiro_id', 'required'),
            array('passageiro_id', 'numerical', 'integerOnly' => true),
            array('nome, bairro, cidade', 'length', 'max' => 60),
            array('rua', 'length', 'max' => 80),
            array('nome', 'enderecoJaInformado'),
            array('numero, coordenada, ponto_referencia', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nome, rua, bairro, cidade, numero, coordenada, passageiro_id, ponto_referencia', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'passageiro' => array(self::BELONGS_TO, 'Passageiro', 'passageiro_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'rua' => 'Rua',
            'bairro' => 'Bairro',
            'cidade' => 'Cidade',
            'numero' => 'Numero',
            'coordenada' => 'Coordenada',
            'passageiro_id' => 'Passageiro',
            'ponto_referencia' => 'Ponto Referencia',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('rua', $this->rua, true);
        $criteria->compare('bairro', $this->bairro, true);
        $criteria->compare('cidade', $this->cidade, true);
        $criteria->compare('numero', $this->numero, true);
        $criteria->compare('coordenada', $this->coordenada, true);
        $criteria->compare('passageiro_id', $this->passageiro_id);
        $criteria->compare('ponto_referencia', $this->ponto_referencia, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getEndererosByPassageiro($passageiro_id)
    {
        $db = Yii::app()->db;

        $sql = "select * from endereco_favorito where passageiro_id = :passageiro_id and ativo = 1";

        $command = $db->createCommand($sql);
        $command->bindParam(':passageiro_id', $passageiro_id, PDO::PARAM_INT);

        return $command->queryAll();
    }

    public function enderecoJaInformado($attributes, $params)
    {

        $condition = 'nome=:nome and numero=:numero and passageiro_id=:passageiro_id and ativo=1';
        
        $params = array(
            ':nome' => strtolower($this->nome),
            ':numero' => strtolower($this->numero),
            ':passageiro_id' => $this->passageiro_id
        );

        if ($this->exists($condition, $params))
        {
            $this->addError('nome', '1');
        }
    }

}

<?php

/**
 * This is the model class for table "recupera_senha".
 *
 * The followings are the available columns in table 'recupera_senha':
 * @property integer $id
 * @property string $chave
 * @property integer $usuario_id
 * @property integer $ativo
 *
 * The followings are the available model relations:
 * @property Usuario $usuario
 */
class RecuperaSenha extends TModel
{

    //public $confirmacao;
    public $verifyCode;

    public function tableName()
    {
        return 'recupera_senha';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('chave, data_requisicao, usuario_email', 'required'),
            array('ativo', 'numerical', 'integerOnly' => true),
            array('chave', 'length', 'max' => 255),
            array('usuario_email', 'length', 'max' => 45),
            array('usuario_email', 'email'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
            array('usuario_email', 'exist', 'attributeName' => 'email', 'className' => 'Usuario', 'message' => 'Email não cadastrado!'),
            array('id, chave, data_requisicao, ativo, usuario_email', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'chave' => 'Chave',
            'data_requisicao' => 'Data Requisicao',
            'ativo' => 'Ativo',
            'usuario_email' => 'Email ',
        );
    }

    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('chave', $this->chave, true);
        $criteria->compare('usuario_id', $this->usuario_id);
        $criteria->compare('ativo', $this->ativa);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function gerarChaveRecuperacao($tamanho = 10, $forca = 8)
    {
        $vogais = 'aeuy';
        $consoantes = 'bdghjmnpqrstvz';
        if ($forca >= 1)
        {
            $consoantes .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($forca >= 2)
        {
            $vogais .= "AEUY";
        }
        if ($forca >= 4)
        {
            $consoantes .= '23456789';
        }
        if ($forca >= 8)
        {
            $vogais .= '@#$%';
        }

        $senha = '';
        $alt = time() % 2;
        for ($i = 0; $i < $tamanho; $i++)
        {
            if ($alt == 1)
            {
                $senha .= $consoantes[(rand() % strlen($consoantes))];
                $alt = 0;
            } else
            {
                $senha .= $vogais[(rand() % strlen($vogais))];
                $alt = 1;
            }
        }
        $this->chave = md5($senha);
    }

    public function validaCodigoAlteracaoSenha()
    {
        $params = array(
            ':email' => trim($this->usuario_email),
            ':chave' => trim($this->chave),
            ':ativo' => 1
        );
        $model = true;

        $model = $this->find('usuario_email = :email AND chave = :chave AND ativo = :ativo', $params);

        return $model;
    }

    public function enviarChave($usuario, $mensagem)
    {
        $mail = new Mail($usuario, utf8_decode('Recuperação de senha'), $mensagem);
    }

    protected function beforeSave()
    {
        $this->updateAll(array('ativo' => 0), 'usuario_email = ?', array($this->usuario_email));
        return parent::beforeSave();
    }

}

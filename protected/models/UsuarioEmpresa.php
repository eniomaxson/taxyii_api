<?php

/**
 * This is the model class for table "usuario_empresa".
 *
 * The followings are the available columns in table 'usuario_empresa':
 * @property integer $id
 * @property integer $usuario_id
 * @property integer $empresa_id
 * @property integer $perfil_id
 * @property integer $administrador
 * @property integer $bloqueio
 * @property string $ultimo_acesso
 *
 * The followings are the available model relations:
 * @property Empresa $empresa
 * @property Usuario $usuario
 */
class UsuarioEmpresa extends TModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'usuario_empresa';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, usuario_id, empresa_id, perfil_id, ultimo_acesso', 'required'),
            array('id, usuario_id, empresa_id, perfil_id, administrador, bloqueio', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, usuario_id, empresa_id, perfil_id, administrador, bloqueio, ultimo_acesso', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empresa' => array(self::BELONGS_TO, 'Empresa', 'empresa_id'),
            'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'usuario_id' => 'Usuario',
            'empresa_id' => 'Empresa',
            'perfil_id' => 'Perfil',
            'administrador' => 'Administrador',
            'bloqueio' => 'Bloqueio',
            'ultimo_acesso' => 'Ultimo Acesso',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('usuario_id', $this->usuario_id);
        $criteria->compare('empresa_id', $this->empresa_id);
        $criteria->compare('perfil_id', $this->perfil_id);
        $criteria->compare('administrador', $this->administrador);
        $criteria->compare('bloqueio', $this->bloqueio);
        $criteria->compare('ultimo_acesso', $this->ultimo_acesso, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UsuarioEmpresa the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}

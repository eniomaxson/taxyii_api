<?php

/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */

/**
 * Description of AcessoFilter
 *
 * @author enio
 */
class AcessoFilter extends CFilter {
	protected function preFilter($filterChain) {
		
		parent::preFilter ( $filterChain );
				
		if (!isset ($_GET['device_id'])) 
			throw new CHttpException(403,CJSON::encode(array('acesso'=>'Acesso nao permitido!')));
			
		$numero_projeto = trim ( $_GET ['device_id'] );
		Seguranca::autenticarAndroid($numero_projeto);
		
		
		$filterChain->run ();
	}
	
	protected function postFilter($filterChain) {
		parent::postFilter ( $filterChain );
	}
}

?>

<?php
class TaxistaController extends TController {
	public function filters() {
		return array (
				array (
						'application.filters.AcessoFilter' 
				) 
		);
		parent::filters ();
	}
	
	// A��o respons�vel por recuperar a posi��o atual do taxista
	public function actionGetPosicao($device_id) {
		$taxista = Taxista::model ()->getPosicaoAtual ();
		if (is_null ( $taxista )) {
			throw new CHttpException(404, CJSON::encode(array('taxista'=> utf8_encode("Taxista n�o encontrado") )) );
		}
		$this->send ( $taxista );
	}
	
	// ��o respon�vel por definir a posi��o atual do taxista
	public function actionSetPosicao($usuario_id, $posicao, $device_id) {
		Taxista::model ()->updateAll ( array (
				'posicao_atual' => $posicao 
				), 'passageiro_id = ?', array (
				$usuario_id 
		));
	}
	
	// A��o respons�vel por adicionar um novo taxista
	public function actionCriarOuAlterar($device_id) {
		$model = new Taxista();
		
		$model->attributes = $this->getRequestPayload();
		
		if(!empty($model->descricao_cidade) && empty($model->cidade_id)){
			$descricao = explode("/", $model->descricao_cidade);
			$cidade = Cidade::model ()->findByAttributes (array ('nome' => trim ( $descricao[0] ),'estado' => trim ( $descricao [1] )));
			
			if(!$cidade){
				$cidade = new Cidade ();
				$cidade->nome = trim ( $descricao [0] );
				$cidade->estado = trim ( $descricao [1] );
				$cidade->save(false);
			}
		}else{
			$cidade = Cidade::model()->findByPk($model->cidade_id);	
		}
		
		$taxista = Taxista::model ()->find ( 'passageiro_id = ?', array ($model->passageiro_id ));
		
		if ($taxista) {
			$taxista->attributes = $model->getAttributes();
			$taxista->cidade_id = $cidade->id;
			if ($taxista->validate ()) {
				$taxista->save ( false );
				$this->send ( $taxista->getAttributes () );
			} else {
				throw new CHttpException(500, CJSON::encode($taxista->getErrors () ));
			}
		} else {
			$model->cidade_id = $cidade->id;
			
			if ($model->validate ()) {
				$model->save (false);
				$this->send ($model->getAttributes());
			} else {
				throw new CHttpException(500, CJSON::encode($taxista->getErrors () ));
			}
		}
	}
	
	// A��o respons�vel por receber a corrida e o id do usuario do taxista para fazer com que o taxista aceite a mesma
	
	public function actionAceitarCorrida($corrida_id, $usuario_id, $device_id) {
		$taxista = new Taxista ();
		
		$taxista->aceitarCorrida ( $corrida_id, $usuario_id );
		
		$this->send(CJSON::encode(array('corrida'=>'Corrida aceita com sucesso!')));
	}
	
	
	#A��o respons�vel por negar uma corrida e j� enviar outra solicita��o para outro taxista
	
	public function actionNegarCorrida($corrida_id,$usuario_id,$device_id) {
		$taxista = new Taxista ();
		
		$taxista->negarCorrida($corrida_id,$usuario_id);
		
		$taxistas_que_negou = $taxista->getListaTaxistaNegou ( $corrida_id );
		
		$corrida = Corrida::model ()->findByPk ( $corrida_id );
		
		$taxista_corrida = $taxista->getTaxiToPassageiro($corrida->ponto_partida, $taxistas_que_negou );
	
		if(!$taxista_corrida)
			throw new CHttpException(404,CJSON::encode(array('taxista'=>utf8_encode('Envio n�o confirmado!')))); 
		
		$taxista->sendPedidoToTaxista ( $taxista_corrida, $corrida);
		
		$this->send(array('corrida'=>'Corrida negada com sucesso!'));
		
	}
	
	#A��o respons�vel por concluir uma corrida liberando o taxista
	
	public function actionFinalizarCorrida($corrida_id,$device_id){
		Taxista::model()->concluirCorrida($corrida_id);
		$this->send(array('corrida'=>'Corrida concluida com sucesso!'));
	}
	
	#Listas as formas de pagamento aceitas por um motorista
	
	public function actionListarFormaPagamento($usuario_id, $device_id) {
		$taxista = Taxista::model ()->find ( 'passageiro_id = ?', array ($usuario_id));
		
		if ($taxista == null)
			throw new CHttpException (404, CJSON::encode (array('taxista_id' => utf8_encode('Taxista n�o encontrado!'))));
		
		$this->send(FormaPagamento::model()->listFrmPagamentoToTaxista ($taxista->id));
	}
	
	public function actionAtualizarFormaPagamento($usuario_id, $device_id) {
		$taxista = Taxista::model ()->find ( 'passageiro_id = ?', array (
				$usuario_id 
		) );
		
		if ($taxista == null)
			throw new CHttpException ( 404, CJSON::encode ( array (
					'taxista_id' => utf8_encode('Taxista n�o encontrado!') 
			) ) );
		
		if (! empty ( $taxista )) {
			$formasPagamentos = $this->getRequestPayload ();
			
			foreach ( $formasPagamentos as $formaPagamento ) {
				$model = FormaPagamento::model ()->findByPk ( $formaPagamento ['id'] );
				
				$ja_aceita = TaxistaFormaPagamento::model ()->exists ( 'forma_pagamento_id = :forma_pagamento_id and taxista_id = :taxista_id', array (
						':forma_pagamento_id' => $model->id,
						':taxista_id' => $taxista->id 
				) );
				
				if ($ja_aceita && $formaPagamento ['aceita'] == '0') {
					TaxistaFormaPagamento::model ()->deleteAll ( 'taxista_id = :taxista_id and forma_pagamento_id = :forma_pagamento_id', array (
							':taxista_id' => $taxista->id,
							':forma_pagamento_id' => $model->id 
					) );
				} else {
					if (! $ja_aceita && $formaPagamento ['aceita'] == '1') {
						$taxistaFrmPgm = new TaxistaFormaPagamento ();
						$taxistaFrmPgm->taxista_id = $taxista->id;
						$taxistaFrmPgm->forma_pagamento_id = $model->id;
						$taxistaFrmPgm->save ( false );
					}
				}
			}
			$this->send ( "OK" );
		}
	}
	public function actionAddTaxi($device_id) {
		$model = new Taxi;
		
		$model->attributes = $this->getRequestPayload ();
		
		$taxista = Taxista::model ()->find ( 'passageiro_id = ?', array ($model->taxista_id));
		
		if(!$taxista)
			throw new CHttpException ( 404, CJSON::encode ( array('taxista'=>utf8_encode('Taxista n�o encontrado!') )));
		
		if (! empty ( $taxista )) {
			$model->taxista_id = $taxista->id;
			if ($model->validate ()) {
				$model->save ( false );
				if ($model->principal) {
					Taxi::model ()->updateAll ( array (
							'principal' => 0 
					), 'taxista_id = :taxista_id and id != :id', array (
							':taxista_id' => $model->taxista_id,
							':id' => $model->id 
					) );
				}
				
				$this->send ( $model->getAttributes () );
			} else {
				throw new CHttpException ( 501, CJSON::encode ( $model->getErrors () ) );
			}
		}
	}
	public function actionListarTaxi($usuario_id, $device_id) {
		$model = Taxista::model ()->find ( 'passageiro_id = ?', array (
				$usuario_id 
		) );
		
		if (! empty ( $model )) {
			$models = Taxi::model ()->listTaxiToTaxista ( $model->id );
			
			if (count ( $models ) > 0) {
				$this->send ( $models );
			} else {
				throw new CHttpException ( 404, CJSON::encode ( array (
						'usuario_id' => utf8_encode("N�o foi encontrado veiculos para esse motorista!") 
				) ) );
			}
		}
	}
	public function actionRemoverTaxi($taxi_id, $device_id) {
		if (! empty ( $taxi_id )) {
			
			if (Taxi::model ()->desativar ( $taxi_id )) {
				$this->send ("OK");
			}
			throw new CHttpException(404,CJSON::encode(array('taxi'=>utf8_encode('Taxi n�o encontrado!'))));
		}
	}
	public function actionAlterarTaxi($taxi_id, $device_id) {
		if (!$taxi_id)
			throw new CHttpException (404, CJSON::encode (array('taxi'=>utf8_encode('Taxi n�o encontrado'))));
			
		$model = Taxi::model ()->findByPk ($taxi_id);
			
		$model->attributes = $this->getRequestPayload ();
			
		try{			
			$model->save ();
				
			if ($model->principal) {
				Taxi::model ()->updateAll ( array ('principal' => 0), 'taxista_id = :taxista_id and id != :id',
											array (':taxista_id' => $model->taxista_id,':id' => $model->id ));
				}
			
			}catch(CException $e){
				throw new CHttpException(500, CJSON::encode($model->getErrors()));	
			}				
			
			$this->send ($model->getAttributes ());
	}	
	
	public function actionGetTaxista($usuario_id,$device_id){

		$taxista = Taxista::model()->find('passageiro_id=?',array($usuario_id));
		
		if(!$taxista)
			throw new CHttpException (404, CJSON::encode (array('taxi'=>utf8_encode('Taxi n�o encontrado'))));
		
		$this->send($taxista->getAttributes());
	}
	
	public function actionListagemCorrida($data_inicio,$data_fim,$usuario_id,$device_id){
	
		$taxista = Taxista::model()->find('passageiro_id=?',array($usuario_id));
		
		if(!$taxista)
			throw new CHttpException(404,CJSON::encode(array('taxista'=>'Taxista n�o encontrado!')));	
		
		if(empty($data_inicio))
			$data_inicio = date('Y-m-d 00:00:01');
		if(empty($data_fim))
			$data_inicio = date('Y-m-d 23:59:00');
		
		$data = $taxista->listagemCorrida($data_inicio,$data_fim,$taxista->id);
		
		$this->send($data);
	}
}

<?php
class UsuarioController extends TController {
	public function filters() {
		return array (
				array (
						'application.filters.AcessoFilter -cadastrar,autenticar,usuarioPorEmail' 
				) 
		);
		parent::filters ();
	}
	
	// ��o respons�vel por permitir cadastro de um novo usuario no sistema
	public function actionCadastrar($device_id) {
		$usuario = new Usuario ('primeiro_acesso');
		
		$usuario->attributes = $this->getRequestPayload ();
		
		if (! $usuario->email)
			throw new CHttpException ( 500, CJSON::encode (array('email' => 'Email deve ser informado!' ) ) );
		
		$usuario->senha = md5 ( $usuario->senha );
		
		if(!empty($usuario->descricao_cidade) && empty($usuario->cidade_id)){
			$descricao = explode("/", $usuario->descricao_cidade);
			$cidade = Cidade::model ()->findByAttributes (array ('nome' => trim ( $descricao[0] ),'estado' => trim ( $descricao [1] )));
			
			if(!$cidade){
				$cidade = new Cidade ();
				$cidade->nome = trim ( $descricao [0] );
				$cidade->estado = trim ( $descricao [1] );
				$cidade->save(false);
			}
		}else{
			$cidade = Cidade::model()->findByPk($usuario->cidade_id);	
		}
		
		$usuario->cidade_id= $cidade->id;
		
		if ($usuario->validate ()) {
			$usuario->save (false);
			
			if($usuario->bloqueio > 0)
			{
				$message = 'Liberar um novo usu�rio! <br> 
				Para liberar acesso para o taxista: ' . $usuario->nome .
				' <a href="http://' . $_SERVER["SERVER_ADDR"] . '/txapi/liberaracesso/' . $usuario->id .'/370978399959"> Clique aqui.</a>';
				$email = new Mail(Usuario::model()->findByPk(1), "Novo taxista", $message);
			}
			
			$this->send ($usuario->getAttributes());
		}
		throw new CHttpException ( 500, CJSON::encode ( $usuario->getErrors () ) );
	}
	
	// A��o responsavel por permitir fazer uma altera��o no registro
	public function actionAlterar($usuario_id, $device_id) {
		$usuario = Usuario::model ()->findByPk ( $usuario_id );
		
		$usuario->setScenario ( "crud" );
		
		$usuario->setAttributes ( $this->getRequestPayload () );
		
		if(!empty($usuario->descricao_cidade) && empty($usuario->cidade_id)){
			$descricao = explode("/", $usuario->descricao_cidade);
			$cidade = Cidade::model ()->findByAttributes (array ('nome' => trim ( $descricao[0] ),'estado' => trim ( $descricao [1] )));
			
			if(!$cidade){
				$cidade = new Cidade ();
				$cidade->nome = trim ( $descricao [0] );
				$cidade->estado = trim ( $descricao [1] );
				$cidade->save(false);
			}
		}else{
			$cidade = Cidade::model()->findByPk($usuario->cidade_id);	
		}
		
		$usuario->cidade_id=$cidade->id;
		
		if ($usuario->validate ()) {
			$usuario->save ( false );
			$this->send ( $usuario->getAttributes () );
		}
		throw new CHttpException ( 500, CJSON::encode ( $usuario->getErrors () ) );
	}
	
	// deletar um usuario no sistema
	public function actionDelete($usuario_id, $device_id) {
		$usuario = Usuario::model ()->findByPk ( $usuario_id );
		try {
			$usuario->delete ();
			$this->send ( $usuario->getAttributes () );
		} catch ( Exception $e ) {
			throw new CHttpException ( 500, CJSON::encode ( $usuario->getErrors () ) );
		}
	}
	
	// ecuperar um usuario no sistema
	public function actionGet($usuario_id, $device_id) {
		$usuario = Usuario::model ()->findByPk ( $usuario_id );
		
		if (! empty ( $usuario ))
			$this->send ( $usuario->attributes );
	}
	
	// ecupera um usuario pelo email
	public function actionUsuarioPorEmail($email,$device_id) {
		$usuario = Usuario::model ()->find ( 'email = ?', array (
				$email 
		) );
		
		if (! empty ( $usuario ))
			$this->send ( $usuario->attributes );
		else
			throw new CHttpException ( 404, CJSON::encode ( array (
					'passageiro' => utf8_encode ( "Passageiro n�o encontrado!" ) 
			) ) );
	}
	
	// Ista todos os usu�rio do sistema
	public function actionListar($device_id) {
		$usuario = new Usuario ();
		
		$this->send ( $usuario->search ()->getData () );
	}
	
	// uncionalidade responsavel por autenticar um usuario no sistema
	public function actionAutenticar($device_id) {
		$usuario = new Usuario ( 'login' );
		
		$usuario->attributes = $this->getRequestPayload ();
		
		$usuario->authenticate ();
		
		if (count ($usuario->getErrors()) > 0) {
			throw new CHttpException (403, CJSON::encode($usuario->getErrors()));
		}
		
		$usuario = Usuario::model ()->find ('email = ?',array($usuario->email));
		
		//$usuario->cidade_id = $usuario->cidade_id ? $usuario->cidade->nome . ' / ' . $usuario->cidade->estado : '';
		
		$this->send ($usuario->getAttributes());
	}
	
	// unctionalidade responsavel por permitir que o passageiro solitite taxi
	public function actionSolicitarCorrida($device_id) {
		$model = new Corrida ();
		
		$model->attributes = $this->getRequestPayload ();
		
		if ($model->passageiro_id == 0)
			Yii::app ()->end ();
		
		if ($model->endereco_favorito_id > 0)
			$endereco = EnderecoFavorito::model ()->findByPk ( $model->endereco_favorito_id );
		
		$ponto_partida = isset ( $endereco ) ? $endereco->coordenada : $model->ponto_partida;
		
		$numero = isset ( $endereco ) ? $endereco->numero : $model->numero;
		
		$ponto_referencia = $model->ponto_referencia;
		
		$taxista = Taxista::model ()->getTaxiToPassageiro ( $ponto_partida );
		
		if (! $taxista)
			throw new CHttpException ( 404, CJSON::encode ( array ('taxista' => utf8_encode ( 'Sem taxista disponivel no momento!' ) ) ) );
		try {
			$model->save ();
			Taxista::model ()->sendPedidoToTaxista ($taxista, $model );
			$this->send($model->getAttributes());
		} catch ( CException $e ) {
			throw new CHttpException (500, CJSON::encode ( $model->getErrors () ) );
		}
	}
	
	// Funcionalidade respons�vel por permitir que os usuario adicionem endere�os
	public function actionAddEndereco($device_id) {
		$model = new EnderecoFavorito ();
		$model->attributes = $this->getRequestPayload ();
		
		// validate = $model->validate () ? true : false;
		try {
			$model->save ();
		} catch ( CException $e ) {
			
			throw new CHttpException ( 500, CJSON::encode ( $model->getErrors () ) );
		}
	}
	
	// Listar os enderecos dos usu�rios do sistema, recebe o id do solicitante
	public function actionListarEndereco($usuario_id, $device_id) {
		$models = EnderecoFavorito::model ()->getEndererosByPassageiro ( $usuario_id );
		
		if (count ( $models ) > 0)
			$this->send ( $models );
		
		throw new CHttpException ( 404, CJSON::encode ( array (
				'endereco' => utf8_encode ( 'N�o foi encontrado nenhum endere�o para o passageiro!' ) 
		) ) );
	}
	
	// exclus�o logica de endere�o
	public function actionRemoveEndereco($endereco_id, $device_id) {
		$model = EnderecoFavorito::model ()->findByPk ( $endereco_id );
		
		if ($model) {
			$model->ativo = 0;
			$model->save ( false );
			$this->send ( $model->getAttributes () );
		}
	}
	
	/*
	 * public function actionTeste() { $apiKey = "AIzaSyC4iEoZ16MyGRx5VRyBzPovSLjoytni91U"; $devices = array('APA91bEB-x7AP4FjxdsIyeNZc7YAUdM62xO_VAlvkvVlmxUAnD3hQi-bx2ljexhJiKqyxS9rSgapyUiGf6TmmqP0GLWNujCocuo3keH4lHsOqTrba_TL3FeRsA-xVYWRnubkOz6JBht3qmW4go92vi57us05fNTZZA'); $message = "Quero um taxi"; $gcm = new GCMPushMessage($apiKey); $gcm->setDevices($devices); $gcm->send($message); } //$apiKey = "AIzaSyCPRWi8TQv_rgw85-2F_d0GFbSo-EEOMeQ"; $devices = array('APA91bHvE2oDBg1IYOAvyuFDrBXAg4Je-plV6Lm0ZEVFMxzWbX_be9MDRZVcfpvTDIF3jp7cqJ1I9ts8FDLwLqFPWV3yIASSonjSlrcRYC8otywga9e9K_Xla18AWAQDbMZ_NpgP8hJLexpOQjj4dHFB06jFjKdl7GNBsEbm2_x1WUgm6zGTkqE'); $message = "Motorista"; $gcm = new GCMPushMessage($apiKey); $gcm->setDevices($devices); echo $gcm->send($message);
	 */
}

?>

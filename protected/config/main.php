<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array (
		'basePath' => dirname ( __FILE__ ) . DIRECTORY_SEPARATOR . '..',
		'name' => 'Taxyii',
		// preloading 'log' component
		'preload' => array (
				'log',
				'bootstrap' 
		),
		'language' => 'pt_br',
		'charset' => 'utf-8',
		'defaultController' => 'principal',
		// autoloading model and component classes
		'import' => array (
				'application.models.*',
				'application.components.*',
				'ext.advancedar.*' 
		),
		'modules' => array (
				'gii' => array (
						'class' => 'system.gii.GiiModule',
						'password' => '1',
						// If removed, Gii defaults to localhost only. Edit carefully to taste.
						'ipFilters' => array (
								'127.0.0.1',
								'::1' 
						),
						'generatorPaths' => array (
								'bootstrap.gii' 
						) 
				) 
		),
		// application components
		'components' => array (
				'user' => array (
						'allowAutoLogin' => true 
				),
				'smtpmail' => array (
						'class' => 'application.extensions.mailer.PHPMailer',
						'Host' => "ssl://smtp.gmail.com",
						'Username' => 'enio.maxson@gmail.com',
						'Password' => 'An0=2O14',
						'Mailer' => 'smtp',
						'Port' => 465,
						'SMTPAuth' => true 
				),
				// uncomment the following to enable URLs in path-format
				'urlManager' => array (
						'urlFormat' => 'path',
						'showScriptName' => false,
						'urlSuffix' => '',
						'rules' => array (
								array ( // OK
										'usuario/listar',
										'pattern' => 'usuarios/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										'usuario/cadastrar',
										'pattern' => 'cadastrarusuario/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										'usuario/delete',
										'pattern' => 'delusuario<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array ( // OK
										'usuario/get',
										'pattern' => 'usuario/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										'usuario/usuarioPorEmail',
										'pattern' => 'usuariopormail/<email>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										'usuario/alterar',
										'pattern' => 'alterarusuario/<usuario_id>/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										// K
										'usuario/autenticar',
										'pattern' => 'autenticar/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										'usuario/listarEndereco',
										'pattern' => 'enderecos/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										'usuario/addEndereco',
										'pattern' => 'addendereco/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										'usuario/removeEndereco',
										'pattern' => 'removerendereco/<endereco_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										'usuario/solicitarCorrida',
										'pattern' => 'solicitarcorrida/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										'taxista/getPosicao',
										'pattern' => 'posicaotaxista/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// K
										'taxista/setPosicao',
										'pattern' => 'atualizarposicao/<usuario_id>/<posicao>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// K
										'taxista/cadastrar',
										'pattern' => 'cadastrartaxista/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/criarOuAlterar',
										'pattern' => 'alterartaxista/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								
								array (
										// OK
										'taxista/getTaxista',
										'pattern' => 'taxista/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/aceitarCorrida',
										'pattern' => 'aceitarcorrida/<corrida_id>/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/negarCorrida',
										'pattern' => 'negarcorrida/<corrida_id>/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/finalizarCorrida',
										'pattern' => 'finalizarcorrida/<corrida_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/listarFormaPagamento',
										'pattern' => 'formaspagamento/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/atualizarFormaPagamento',
										'pattern' => 'atualizarformapagamento/<usuario_id>/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/addTaxi',
										'pattern' => 'addtaxi/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/listarTaxi',
										'pattern' => 'listartaxi/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/removerTaxi',
										'pattern' => 'removertaxi/<taxi_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								),
								array (
										// OK
										'taxista/alterarTaxi',
										'pattern' => 'alterartaxi/<taxi_id>/<device_id>',
										'verb' => 'POST',
										'parsingOnly' => true 
								),
								array(
										// OK
										'taxista/listagemCorrida',
										'pattern' => 'listarcorrida/<data_inicio>/<data_fim>/<usuario_id>/<device_id>',
										'verb' => 'GET',
										'parsingOnly' => true 
								)  
						) 
				),
				// uncomment the following to use a MySQL database
				'db' => array (
						'connectionString' => 'mysql:host=localhost;dbname=taxyii_db',
						'emulatePrepare' => true,
						'username' => 'root',
						'password' => 'An0=2O13',
						'charset' => 'utf8' 
				),
				'errorHandler' => array (
						// use 'site/error' action to display errors
						'errorAction' => 'principal/error' 
				),
				'log' => array (
						'class' => 'CLogRouter',
						'routes' => array (
								array (
										'class' => 'CFileLogRoute',
										'levels' => 'error, warning' 
								) 
						) 
				),
				'request' => array (
						'enableCookieValidation' => true 
				) 
		),
		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params' => array (
				// this is used in contact page
				'adminEmail' => 'enio.maxson@gmail.com',
				'apikey_passageiro' => 'AIzaSyAsLGGTHvU-s-RKEKDmMHg22YS4nKJb05k',
				'apikey_taxista' => 'AIzaSyC4iEoZ16MyGRx5VRyBzPovSLjoytni91U',
				'project_passageiro' => sha1 ( '370978399959' ),
				'project_taxista' => sha1 ( '49105500219' ) 
		) 
);

//mailer
